# bike_ble

A Flutter Application with BLE functionality to connect to a Peripheral to unlock it.
It is developed for Marquardt GmbH in order to fetch data from Ubstack which is distributed from Fraunhofer IAO.
All API request templates are accessible on Fraunhofer IAO development website.

## current status of development

Implemented is the fetching of data after Login without JWT refreshing, the illustration of bookings in present or future and a manual connecting via BLE to a Peripheral.

