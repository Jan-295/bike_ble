import 'package:flutter/material.dart';




//////////////////////CUSTOM_COLORS/////////////////////

class MQColors {
  static const mqBlueGreen = const Color.fromRGBO(0,154,155,1);
  static const mqBlueGreenDark1 = const Color.fromRGBO(0,116,116,1);
  static const mqBlueGreenDark2 = const Color.fromRGBO(0,77,78,1);
  static const mqGrey = const Color.fromRGBO(70, 70, 70, 1);

  const MQColors();
}

/////////////////////TEXT_STYLES///////////////////////

TextStyle vehiclesCardStyleTitle(){
  return TextStyle(
      fontSize: 15,
      color: MQColors.mqBlueGreenDark2,
  );
}

TextStyle vehiclesCardStyleText(){
  return TextStyle(
    fontSize: 13,
    color: MQColors.mqBlueGreenDark2,
  );
}
