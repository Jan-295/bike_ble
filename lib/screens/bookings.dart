// @dart=2.9

import 'package:bike_ble/screens/ble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bike_ble/style.dart';
import 'package:intl/intl.dart';
import 'data/ubstackClasses.dart';
import 'dart:async';
import 'data/data.dart';
import 'ble.dart';
import 'package:expansion_card/expansion_card.dart';

class Bookings extends StatefulWidget {
  @override
  _BookingsState createState() => _BookingsState();
}

class _BookingsState extends State<Bookings> {
  Future<List<Booking>> fetchBookings;
  Future<List<ActiveVehicle>> fetchVehicles;
  Map<String, String> header;

  @override
  void initState() {
    super.initState();
    fetchBookings = getBookings();
    fetchVehicles = getActiveVehicles();
    _getAccessHeader();
  }

  void _getAccessHeader() async {
    header = await getAccessHeader();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            MQColors.mqBlueGreen,
            MQColors.mqBlueGreenDark2,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: RefreshIndicator(
          color: Colors.white70,
          backgroundColor: MQColors.mqBlueGreenDark1,
          onRefresh: () => _refreshWidget(),
          child: FutureBuilder(
            future: fetchBookings,
            builder: (context, bookings) {
              //_refreshWidget();
              if (bookings.hasError) {
                return Center(
                  child: Text("No data available\nError: ${bookings.error}"),
                );
              } else if (bookings.hasData) {
                if (bookings.data.length == 0) {
                  return noBookingCard(context);
                }
                return ListView.builder(
                    itemCount: bookings.data.length,
                    itemBuilder: (context, index) {
                      return bookingCard(
                          bookings.data[index], context, header, fetchVehicles);
                    });
              }

              return Center(
                child:
                    CircularProgressIndicator(color: MQColors.mqBlueGreenDark2),
              );
            },
          ),
        ),
      ),
    );
  }

  Future _refreshWidget() async {
    await Future.delayed(Duration(milliseconds: 200));
    final data1 = getBookings();
    final data2 = getActiveVehicles();
    setState(() {
      fetchBookings = data1;
      fetchVehicles = data2;
    });
  }
}

Widget noBookingCard(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      color: Colors.white70,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            "no booking available",
            style: TextStyle(color: MQColors.mqBlueGreenDark2),
          ),
        )
      ]),
    ),
  );
}

Widget bookingCard(Booking booking, BuildContext context,
    Map<String, String> header, Future<List<ActiveVehicle>> vehicles) {
  Function _showButton(Booking booking) {
    if (booking.startDateTime.isBefore(DateTime.now())) {
      return () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => StartBLE()));
      };
    } else {
      return null;
    }
  }

  Widget FetchImage() {
    return FutureBuilder(
        future: vehicles,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            for (ActiveVehicle v in snapshot.data) {
              if (v.id == booking.vehicleId) {
                return Image.network(
                  v.images,
                  headers: header,
                  width: 100,
                  alignment: Alignment.center,
                );
              }
            }
          }
          return CircularProgressIndicator();
        });
  }

  return Padding(
    padding: const EdgeInsets.all(10.0),
    child: Card(
      color: Colors.white70,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          FetchImage(),
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Text(booking.reason, style: vehiclesCardStyleTitle(),),
            SizedBox(height: 7,),
            Text("Start: ${DateFormat('dd-MM-yyyy – HH:mm').format(booking.startDateTime.toLocal())} Uhr",style: vehiclesCardStyleText(),),
            SizedBox(height: 3,),
            Text("End: ${DateFormat('dd-MM-yyyy – HH:mm').format(booking.endDateTime.toLocal())} Uhr",style: vehiclesCardStyleText(),),
            SizedBox(height: 3,),
            RaisedButton(
              color: MQColors.mqBlueGreenDark1,
              onPressed: _showButton(booking),
              child: Text(
                'Activate',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ]),

        ]),
      ),
    ),
  );
}
