// @dart=2.9

import 'package:bike_ble/screens/ble.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bike_ble/style.dart';
import 'package:intl/intl.dart';
import 'data/ubstackClasses.dart';
import 'dart:async';
import 'data/data.dart';
import 'ble.dart';
import 'package:expansion_card/expansion_card.dart';

class Version extends StatefulWidget {
  @override
  _VersionState createState() => _VersionState();
}

class _VersionState extends State<Version> {
  Future<List<Booking>> fetchBookings;
  Future<List<ActiveVehicle>> fetchVehicles;
  Map<String, String> header;

  @override
  void initState() {
    super.initState();
    fetchBookings = getBookings();
    fetchVehicles = getActiveVehicles();
    _getAccessHeader();
  }

  void _getAccessHeader() async {
    header = await getAccessHeader();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            MQColors.mqBlueGreen,
            MQColors.mqBlueGreenDark2,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Text(
          "Version: v1.0",
          style: TextStyle(fontSize: 20, color: Colors.white70),
        )
      ),
    );
  }
}