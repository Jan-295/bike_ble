import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bike_ble/style.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'ble_widgets.dart';
import 'dart:math';
import 'dart:async';
import 'package:collection/collection.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'data/data.dart';

class StartBLE extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StreamBuilder<BluetoothState>(
          stream: FlutterBlue.instance.state,
          initialData: BluetoothState.unknown,
          builder: (c, snapshot) {
            final state = snapshot.data;
            if (state == BluetoothState.on) {
              return FindDevicesScreen();
            }
            return BluetoothOffScreen(state: state);
          }),
    );
  }
}

class BluetoothOffScreen extends StatelessWidget {
  const BluetoothOffScreen({Key? key, this.state}) : super(key: key);

  final BluetoothState? state;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            MQColors.mqBlueGreen,
            MQColors.mqBlueGreenDark2,
          ],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
        )),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.bluetooth_disabled,
                size: 200.0,
                color: Colors.white70,
              ),
              Text(
                'Bluetooth Adapter is ${state != null ? state.toString().substring(15) : 'not available'}.',
                style: Theme.of(context)
                    .primaryTextTheme
                    .subhead
                    ?.copyWith(color: Colors.white70),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FindDevicesScreen extends StatelessWidget {
  _filter(c, snapshot) {
    print("print${snapshot.data.toString()}");
    int printed = 0;
    if (snapshot.data!.length > 0) {
      for (ScanResult r in snapshot.data!) {
        if (r.device.name.length > 0) {
          printed++;
          return ScanResultTile(
            result: r,
            onTap: () =>
                Navigator.of(c).push(MaterialPageRoute(builder: (context) {
              r.device.connect();
              return DeviceScreen(device: r.device);
            })),
          );
        }
      }
      if (printed <= 0) {
        return Center(
            child: Column(children: [
          SizedBox(
            height: 20,
          ),
          Text("no devices found from scanning"),
        ]));
      }
    } else {
      return Center(
          child: Column(children: [
        SizedBox(
          height: 20,
        ),
        Text("no devices found from scanning"),
      ]));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            MQColors.mqBlueGreen,
            MQColors.mqBlueGreenDark2,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          foregroundColor: MQColors.mqBlueGreen,
          backgroundColor: MQColors.mqBlueGreen,
          title: Text("Find and Connect"),
          automaticallyImplyLeading: true,
        ),
        body: RefreshIndicator(
          onRefresh: () =>
              FlutterBlue.instance.startScan(timeout: Duration(seconds: 4)),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                StreamBuilder<List<BluetoothDevice>>(
                  stream: Stream.periodic(Duration(seconds: 2))
                      .asyncMap((_) => FlutterBlue.instance.connectedDevices),
                  initialData: [],
                  builder: (c, snapshot) => Column(
                    children: snapshot.data!
                        .map((d) => ListTile(
                              title: Text(d.name),
                              subtitle: Text(d.id.toString()),
                              trailing: StreamBuilder<BluetoothDeviceState>(
                                stream: d.state,
                                initialData: BluetoothDeviceState.disconnected,
                                builder: (c, snapshot) {
                                  if (snapshot.data ==
                                      BluetoothDeviceState.connected) {
                                    return RaisedButton(
                                      child: Text('OPEN'),
                                      onPressed: () => Navigator.of(context)
                                          .push(MaterialPageRoute(
                                              builder: (context) =>
                                                  DeviceScreen(device: d))),
                                    );
                                  }
                                  return Text(snapshot.data.toString());
                                },
                              ),
                            ))
                        .toList(),
                  ),
                ),
                StreamBuilder<List<ScanResult>>(
                    stream: FlutterBlue.instance.scanResults,
                    initialData: [],
                    builder: (c, snapshot) => _filter(c, snapshot)),

              ],
            ),
          ),
        ),
        floatingActionButton: StreamBuilder<bool>(
          stream: FlutterBlue.instance.isScanning,
          initialData: false,
          builder: (c, snapshot) {
            if (snapshot.data!) {
              return FloatingActionButton(
                child: Icon(Icons.stop),
                onPressed: () => FlutterBlue.instance.stopScan(),
                backgroundColor: Colors.red,
              );
            } else {
              return FloatingActionButton(
                child: Icon(Icons.search),
                onPressed: () => FlutterBlue.instance
                    .startScan(timeout: Duration(seconds: 10)),
                backgroundColor: MQColors.mqBlueGreenDark1,
              );
            }
          },
        ),
      ),
    );
  }
}

class DeviceScreen extends StatelessWidget {
  DeviceScreen({Key? key, required this.device}) : super(key: key);

  final BluetoothDevice device;
  List<BluetoothService> services = [];
  List<int> keyWriteValue = [0x05];
  final Map<Guid, List<int>> readValues = new Map<Guid, List<int>>();

  Future<List<int>> readVal(BluetoothCharacteristic c) async {
    return await c.read();
  }

  Widget activateButton(BluetoothService s) {
    for (BluetoothCharacteristic c in s.characteristics) {
      if (c.uuid == activateCharacteristic) {
        return LiteRollingSwitch(
          value: false,
          iconOff: Icons.power_settings_new,
          textOff: "inactive",
          textOn: "active",
          onChanged: (bool state) async {
            state ? c.write(keyWriteValue) : c.write([0x00]);
          },
        );
      }
    }
    return Text("no char");
  }

  void _waitForServices() async {
    await device.discoverServices();
  }

  BluetoothCharacteristic getCharacteristic(BluetoothService s, Guid uuid) {
    for (BluetoothCharacteristic c in s.characteristics) {
      if (c.uuid == uuid) {
        return c;
      }
    }
    return s.characteristics[0];
  }

  void setNotify(BluetoothCharacteristic c) async {
    c.value.listen((value) {
      readValues[c.uuid] = value;
    });
    await c.setNotifyValue(!c.isNotifying);
    await c.read();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            MQColors.mqBlueGreen,
            MQColors.mqBlueGreenDark2,
          ],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          foregroundColor: MQColors.mqBlueGreen,
          backgroundColor: MQColors.mqBlueGreen,
          title: Text(device.name),
          actions: <Widget>[
            StreamBuilder<BluetoothDeviceState>(
              stream: device.state,
              initialData: BluetoothDeviceState.connecting,
              builder: (c, snapshot) {
                VoidCallback? onPressed;
                String text;
                switch (snapshot.data) {
                  case BluetoothDeviceState.connected:
                    onPressed = () => device.disconnect();
                    text = 'DISCONNECT';
                    break;
                  case BluetoothDeviceState.disconnected:
                    onPressed = () => device.connect();
                    text = 'CONNECT';
                    break;
                  default:
                    onPressed = null;
                    text = snapshot.data.toString().substring(21).toUpperCase();
                    break;
                }
                return FlatButton(
                  onPressed: onPressed,
                  child: Text(text),
                );
              },
            )
          ],
        ),
        body: StreamBuilder<BluetoothDeviceState>(
            stream: device.state,
            initialData: BluetoothDeviceState.connecting,
            builder: (c, snapshot) {
              while (snapshot.data == BluetoothDeviceState.connected) {
                _waitForServices();
                return StreamBuilder<bool>(
                  stream: device.isDiscoveringServices,
                  initialData: false,
                  builder: (c, snapshot) => IndexedStack(
                    index: snapshot.data! ? 1 : 0,
                    children: <Widget>[
                      StreamBuilder<List<BluetoothService>>(
                          stream: device.services,
                          initialData: [],
                          builder: (c, snapshot) {
                            print(snapshot.connectionState);
                            if (snapshot.connectionState ==
                                ConnectionState.active) {
                              services = snapshot.data!;
                              for (BluetoothService s in snapshot.data!) {
                                if (s.uuid == bikeService) {
                                  return Column(children: [
                                    Container(
                                      alignment: Alignment.center,
                                      padding: const EdgeInsets.all(10.0),
                                      child: activateButton(s),
                                    ),
                                    /*StreamBuilder(
                                      stream: Stream.fromFuture(getCharacteristic(s, readTest).read()),
                                      initialData: 0,
                                      builder: (context, snapshot) {
                                        //setNotify(getCharacteristic(s, readTest));
                                        //print(snapshot.connectionState);

                                        return Text(snapshot.data!.toString());

                                        //return CircularProgressIndicator();
                                      }
                                  ),*/
                                  ]);
                                }
                              }
                            }
                            return CircularProgressIndicator(
                              color: MQColors.mqBlueGreenDark1,
                            );
                          }),
                      CircularProgressIndicator(
                        color: MQColors.mqBlueGreenDark1,
                      )
                    ],
                  ),
                );
              }
              return Container(
                height: 0,
                width: 0,
              );
            }),
      ),
    );
  }
}
