// @dart=2.9

import 'package:bike_ble/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'data/ubstackClasses.dart';
import 'data/data.dart';
import 'dart:async';
import 'package:expansion_card/expansion_card.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Vehicles extends StatefulWidget {
  @override
  _VehiclesState createState() => _VehiclesState();
}

class _VehiclesState extends State<Vehicles> {
  Future<List<ActiveVehicle>> fetchVehicles;
  Map<String, String> header;

  @override
  void initState() {
    super.initState();
    fetchVehicles = getActiveVehicles();
    _getAccessHeader();
  }

  void _getAccessHeader() async {
    header = await getAccessHeader();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            MQColors.mqBlueGreen,
            MQColors.mqBlueGreenDark2,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: FutureBuilder(
          future: fetchVehicles,
          builder: (context, vehicles) {
            if (vehicles.hasError) {
              return Center(
                child: Text("No data available\nError: ${vehicles.error}"),
              );
            } else if (vehicles.hasData) {
              return RefreshIndicator(
                color: Colors.white70,
                backgroundColor: MQColors.mqBlueGreenDark1,
                onRefresh: () => fetchVehicles = getActiveVehicles(),
                child: ListView.builder(
                    itemCount: vehicles.data.length,
                    itemBuilder: (context, index) {
                      return vehicleCard(vehicles.data[index], header);
                    }),
              );
            }

            return Center(
              child:
                  CircularProgressIndicator(color: MQColors.mqBlueGreenDark2),
            );
          },
        ),
      ),
    );
  }
}

Widget vehicleCard(ActiveVehicle vehicle, Map<String, String> header) {

  _launchApp() async {
    if (await canLaunch(urlApp)) {
      await launch(urlApp);
    } else {
      throw 'Could not launch $urlApp';
    }
  }

  return Padding(
    padding: const EdgeInsets.all(10.0),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white70,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: ExpansionCard(
        trailing: Icon(
          Icons.keyboard_arrow_down,
          color: MQColors.mqBlueGreenDark1,
        ),
        title: Center(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.network(
                    vehicle.images,
                    headers: header,
                    width: 100,
                    alignment: Alignment.center,
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(vehicle.manufacturer,
                                  style: vehiclesCardStyleTitle(),
                                  overflow: TextOverflow.ellipsis),
                              SizedBox(width: 10),
                              Text(
                                vehicle.modelName,
                                style: vehiclesCardStyleTitle(),
                                overflow: TextOverflow.visible,
                                maxLines: 2,
                                softWrap: false,
                              ),
                            ]),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                vehicle.type,
                                style: vehiclesCardStyleText(),
                              ),
                              SizedBox(width: 10),
                              Text(vehicle.category,
                                  style: vehiclesCardStyleText(),
                                  overflow: TextOverflow.ellipsis),
                            ]),
                        Text(vehicle.licensePlateNumber,
                            style: vehiclesCardStyleText())
                      ]),
                ]),
          ),
        ),
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 5.0, 15.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Seats: ${vehicle.seats}",
                          style: vehiclesCardStyleText(),
                        ),
                        Text(
                          "Driving range: ${vehicle.range}",
                          style: vehiclesCardStyleText(),
                        ),
                        Text(
                          "KM driven: ${vehicle.odometer}",
                          style: vehiclesCardStyleText(),
                        ),
                      ]),
                  FlatButton(
                    disabledColor: MQColors.mqBlueGreenDark1,
                    color: MQColors.mqBlueGreenDark1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    child: Text(
                      "Book",
                      style: TextStyle(color: Colors.white70),
                    ),
                    onPressed: () => _launchApp()
                  ),
                ]),
          ),
        ],
      ),
    ),
  );

}
