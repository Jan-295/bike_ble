// @dart=2.9
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'ubstackClasses.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


final storage = new FlutterSecureStorage();

/////////////////////////////HEADERS & BODIES FOR HTTP REQUESTS////////////////////////////////////////
Map<String, String> loginHeader = {
  "Content-type": "application/json",
  "Accept": "application/json"
};

Future <Map<String, String>> getAccessHeader() async{

  Map<String, String> accessHeader = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer ${await storage.read(key: "jwt")}',
  };
  return accessHeader;
}

///////////////////////////BLE UUIDs///////////////////////////////////////////////////////////////////

//:TODO change uuids for actual ones

Guid bikeService = Guid("19b10000-e8f2-537e-4f6c-d104768a1214");
Guid activateCharacteristic = Guid("19b10001-e8f2-537e-4f6c-d104768a1214");
Guid batteryStateOfChargeCharacteristic = Guid("8888");
Guid actualSpeed = Guid("9999");
Guid readTest = Guid("19b10002-e8f2-537e-4f6c-d104768a1214");

///////////////////////////URLS FOR HTTP REQUESTS//////////////////////////////////////////////////////

final String baseUrl = 'https://ubstack.iao.fraunhofer.de';
final String authenticationUrl = '/authentication/api/authenticate';
final String activeVehiclesUrl = '/coredatavehicle/api/vehicles?active=true';
final String bookingsUrl = '/coredatabooking/api/own/singlebookings?onShow=true';
final String tenantConfig = '/coredatauserconfiguration/api/tenantconfiguration';
const urlApp = 'https://ubstack.iao.fraunhofer.de/app';

//////////////////////////GETTER FOR API VEHICLE DATA/////////////////////////////////////////////////
Future<List<ActiveVehicle>> getActiveVehicles() async {
  List jsonVehicles = [];
  List<ActiveVehicle> vehicles = [];
  final response = await http.get(baseUrl + activeVehiclesUrl, headers: await getAccessHeader());

  if (response.statusCode == 200 || response.statusCode == 201) {
    if (response.body.isNotEmpty) {
      jsonVehicles = jsonDecode(response.body);
      print(jsonVehicles[0]);

      for (int v = 0; v < jsonVehicles.length; v++) {

        vehicles.add(ActiveVehicle.fromJson(jsonVehicles[v]));
      }
      return vehicles;
    } else {
      throw Exception();
    }
  } else {
    print("vehicles fetch: ${response.statusCode}");
    throw Exception();
  }
}

Future<List<Booking>> getBookings() async {
  List jsonBookings = [];
  List<Booking> bookings = [];
  final response = await http.get(baseUrl + bookingsUrl, headers: await getAccessHeader());

  if (response.statusCode == 200 || response.statusCode == 201) {
    if (response.body.isNotEmpty) {
      jsonBookings = jsonDecode(response.body);
      print(jsonBookings[0]);
      var endDateTime;
      for (int b = 0; b < jsonBookings.length; b++) {
        endDateTime = DateTime.parse(jsonBookings[b]["endTimePlanned"]);
        if(endDateTime.isAfter(DateTime.now()) && (jsonBookings[b]["status"]=="FIXED"||jsonBookings[b]["status"]=="RESERVED")) { // add to list if booking is fixed (not terminated) and if its end date hasnt expired
          bookings.add(Booking.fromJson(jsonBookings[b]));
        }
      }
      print(bookings.length);
      return bookings;
    } else {
      throw Exception();
    }
  } else {
    print("bookings fetch: ${response.statusCode}");
    throw Exception();
  }
}
