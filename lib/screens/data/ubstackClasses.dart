// @dart=2.9

////////////////////////////ACTIVE_VEHICLE_CLASS///////////////////////////////

class ActiveVehicle {
  final String id;
  final String manufacturer;
  final String modelName;
  final String licensePlateNumber;
  final String securedObjectId;
  final double range;
  final int seats;
  final double odometer;
  final String images;
  final String type;
  final String category;

  ActiveVehicle({
      this.id,
      this.securedObjectId,
      this.manufacturer,
      this.modelName,
      this.licensePlateNumber,
      this.range,
      this.seats,
      this.odometer,
      this.images,
      this.type,
      this.category
  });

  factory ActiveVehicle.fromJson(final json) {
    return ActiveVehicle(
        id: json["id"],
        securedObjectId: json["securedObjectId"],
        manufacturer: json["vehicleModel"]["manufacturer"],
        modelName: json["vehicleModel"]["modelName"],
        licensePlateNumber: json["licensePlateNumber"],
        range: json["adjustedElectricDrivingRange"],
        seats: json["seats"],
        odometer: json["odometerReading"],
        type: json["vehicleModel"]["vehicleType"],
        images: json["vehicleModel"]["vehicleCategory"]["imageURL"],
        category: json["vehicleModel"]["vehicleCategory"]["name"]
    );
  }
}

////////////////////////////BOOKING_CLASS///////////////////////////////

class Booking{
  final String reason;
  var startDateTime;
  var endDateTime;
  final String status;
  var vehicleId;


  Booking({
    this.reason,
    this.startDateTime,
    this.endDateTime,
    this.status,
    this.vehicleId
  });

  factory Booking.fromJson(final json){
    return Booking(
        reason: json["reason"],
        startDateTime: DateTime.parse(json["startTimePlanned"]),
        endDateTime: DateTime.parse(json["endTimePlanned"]),
        status: json["status"],
        vehicleId: json["vehicleId"],
    );
  }
}

