// @dart=2.9

import 'package:bike_ble/style.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'package:bike_ble/navigation.dart';
import 'data/data.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final storage = new FlutterSecureStorage();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              MQColors.mqBlueGreen,
              MQColors.mqBlueGreenDark2,
            ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: _isLoading ? Center(child: CircularProgressIndicator(color: MQColors.mqBlueGreenDark2,)) : ListView(
        children: <Widget>[
          headerSection(),
          textSection(),
          bottomSection(),
        ]
      )
    ),
    );
  }

  signIn(String email, password) async {
    Map<String, String> data = {
      "username": email,
      "password": password
    };
    var jsonData = null;
    final String apiUrl = baseUrl + authenticationUrl;
    final response = await http.post(apiUrl,headers: loginHeader,body: jsonEncode(data)); //wait on respond of ubstack api
    
    if(response.statusCode == 200||response.statusCode == 201) {
      jsonData = json.decode(response.body);
      await storage.write(key: "jwt", value: jsonData["jwt"]);
      setState(() {
        _isLoading = false;
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Navigation()), ((Route<dynamic> route) => false));
      });
    }
    else {
      print("API request ended with Code : ${response.statusCode}");
    }
  }

  Container bottomSection() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 40.0,
      padding: EdgeInsets.symmetric(horizontal: 30.0),
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
        onPressed: () {
          setState(() {
            _isLoading = true;
          });
          signIn(emailController.text, passwordController.text);
        },
        color: MQColors.mqBlueGreenDark2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Text("Sign In", style: TextStyle(color: Colors.white70),
      ),
      ),
    );
  }

  Container textSection(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: <Widget>[
          txtEmail("Email", Icons.email),
          SizedBox(height: 30.0),
          txtPassword("Password", Icons.lock),
        ]
      ),
    );
  }

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  TextFormField txtPassword(String title, IconData icon) {
    return TextFormField(
      controller: passwordController,
      obscureText: true,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon, color: MQColors.mqBlueGreenDark2,),
      ),
    );
  }

  TextFormField txtEmail(String title, IconData icon) {
    return TextFormField(
      controller: emailController,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
        hintText: title,
        hintStyle: TextStyle(color: Colors.white70),
        icon: Icon(icon, color: MQColors.mqBlueGreenDark2,),
      ),
    );
  }
  
  Container headerSection() {
    return Container(
      padding: EdgeInsets.fromLTRB(20.0, 80.0, 20.0, 50.0),
      child: Row(children: [
        Image.asset('assets/images/Marquardt_Logo_MQB.png', scale: 10, color: Colors.white),
        SizedBox(width: 10,),
        Text("Ubstack Login", style: TextStyle(color: Colors.white, fontSize: 35.0)),
      ],
      ),
    );
  }
}
