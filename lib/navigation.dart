// @dart=2.9
import 'dart:io';

import 'package:bike_ble/screens/version.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'style.dart';
import 'screens/vehicles.dart';
import 'screens/bookings.dart';
import 'screens/login.dart';
import 'screens/ble.dart';
import 'screens/data/ubstackClasses.dart';

class Navigation extends StatefulWidget {

  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  final storage = FlutterSecureStorage();
  int _currentIndex = 1;
  final tabs =[
    Center(child: Version()),
    Center(child: Vehicles()),
    Center(child: Bookings()),
  ];

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    if(await storage.read(key: "jwt") == null){
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), ((Route<dynamic> route) => false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(

        backgroundColor: MQColors.mqBlueGreen,
        title: Row( children:[
                        Image.asset('assets/images/Marquardt_Logo_1.png',height: 60, color: Colors.white,),
                        SizedBox(width: 20,),
                        Text("ubstack"),
        ]
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () async {
              await storage.delete(key: "jwt");
              Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), ((Route<dynamic> route) => false));
            },
            child: Row( children: [
              Text("Logout", style: TextStyle(color: Colors.white)),
              SizedBox(width: 10),
              Icon(Icons.logout, color: Colors.white,),
            ],
            ),
          ),
        ],
      ),

      body: Container(
        color: MQColors.mqGrey,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: tabs[_currentIndex],
      ),

     // drawer: Drawer(),

      bottomNavigationBar: CurvedNavigationBar(
        color: MQColors.mqBlueGreenDark1,
        backgroundColor: MQColors.mqBlueGreenDark2,
        buttonBackgroundColor: MQColors.mqBlueGreenDark1,
        height: 55,
        items: <Widget>[
          Icon(Icons.person,size: 20, color: MQColors.mqBlueGreenDark2),
          Icon(Icons.directions_car_sharp,size: 20, color: MQColors.mqBlueGreenDark2),
          Icon(Icons.book_outlined,size: 20, color: MQColors.mqBlueGreenDark2),
        ],
        animationDuration: Duration(milliseconds: 100), //Duration of the navigation animation
        animationCurve: Curves.bounceInOut, //select animation
        index: 0, //starting icon -> Home
        onTap: (index){
          debugPrint("Current Index is $index");
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}